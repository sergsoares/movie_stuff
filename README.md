# README

This is a example application based on this Dribbble layout
[Movie & TV Show Trailer Concept](https://dribbble.com/shots/2535150-Movie-TV-Show-Trailer-Concept)

### API for movies information

- https://www.omdbapi.com
- https://www.themoviedb.org/documentation/api
    - https://github.com/18Months/themoviedb-api/


Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
